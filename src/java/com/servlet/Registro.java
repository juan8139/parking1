/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Statement;
import java.sql.SQLException;

/**
 *
 * @author jptorres
 */
public class Registro extends HttpServlet {
    java.sql.Connection conexion = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            String pass1 = request.getParameter("Pass1");
            String pass2 = request.getParameter("Pass2");
            try{
             Class.forName("com.mysql.jdbc.Driver");
             conexion = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/datos","root","123456");
             Statement s = conexion.createStatement();
             if(pass1.equals(pass2)){
                 s.executeUpdate("INSERT INTO USUARIOS (NOMBRE,USUARIO,PASSWORDD) VALUES('"+request.getParameter("name")+"','"+ request.getParameter("User")+"','"+pass1+"')");
                 //mensaje.println("INSERT INTO USUARIOS VALUES('"+ request.getParameter("nombre")+"','"+ request.getParameter("usuario")+"','"+request.getParameter("pass")+"')");
                 out.println("insercion correcta");
             }else{
                 out.println("Las Contraseñas no coinciden");
             }
                 
            }catch(SQLException e){
               e.printStackTrace();
            }catch(ClassNotFoundException e){
                e.printStackTrace();
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
