/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.servlet.http.HttpSession;
/**
 *
 * @author juanp
 */
public class Myservlet extends HttpServlet {
    
    private java.sql.Connection conexion = null;
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                 HttpSession sesion = request.getSession();
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            PrintWriter mensaje = response.getWriter();
                try{
                   
                    Class.forName("com.mysql.jdbc.Driver");
                    conexion = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/datos","root","123456");
                    Statement s = conexion.createStatement();
                    //mensaje.println("INSERT INTO USUARIOS VALUES('"+ request.getParameter("nombre")+"','"+ request.getParameter("usuario")+"','"+request.getParameter("pass")+"')");
                    ResultSet rs = s.executeQuery("Select usuario,passwordd from USUARIOS Where usuario ='"+ request.getParameter("usuario")+"' AND passwordd = '"+request.getParameter("pass")+"'");
                    //mensaje.println("Select usuario,passwordd from USUARIOS Where usuario ='"+ request.getParameter("usuario")+"' AND passwordd = '"+request.getParameter("pass")+"'");
                            int con = 0;
                    while (rs.next()){
                        con = con + 1;
                    }
                    //mensaje.println(con);
                    //sesion.invalidate();
                    sesion.setAttribute("usuario",null);
                    if (con > 0 && con < 2 && sesion.getAttribute("usuario") == null){
                            mensaje.println("exito");
                            sesion.setAttribute("usuario", request.getParameter("usuario"));
                            response.sendRedirect("prueba.jsp");
                    }else{
                        //mensaje.println("<script language=\"javascript\"> alert(\"Bienvenido\"); </script>");
                        mensaje.println("<script type=\"text/javascript\">");
                        mensaje.println("function alerta(){alert('User or password incorrect');}");
                        mensaje.println("</script>");
                        mensaje.println("<html><head></head><body onload=\"alerta()\"></body></html>");
                        response.sendRedirect("index.jsp");
                    }
                    conexion.close();
                }
                catch(SQLException e){
                    e.printStackTrace();
                }
                catch(ClassNotFoundException e){
                    e.printStackTrace();    
                }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
